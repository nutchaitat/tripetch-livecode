part of 'application.dart';

final class AppTheme {
  final Color colorAppBar = Colors.green;
  final Color colorCard = Colors.blue;
  final Color colorText = Colors.white;

  ThemeData themeData(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: colorAppBar));
    return Theme.of(context).copyWith(
      useMaterial3: true,
      brightness: Brightness.light,
      primaryColor: colorAppBar,
      appBarTheme: AppBarTheme(
        color: colorAppBar,
        titleTextStyle: TextStyle(color: colorText, fontSize: 20),
      ),
    );
  }
}

var themeProvider = Provider((ref) {
  return AppTheme();
});
