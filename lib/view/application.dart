import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tripetch_livecode/view/router.dart';

part 'application.theme.dart';

class Application extends ConsumerWidget {
  const Application({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final router = ref.read(routerProvider);
    final theme = ref.read(themeProvider);
    
    return MaterialApp.router(
      routerConfig: router,
      theme: theme.themeData(context),
    );
  }
}
