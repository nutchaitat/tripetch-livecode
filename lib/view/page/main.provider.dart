part of 'main.dart';

@freezed
class MainState with _$MainState {
  factory MainState({
    @Default(AsyncValue.loading()) AsyncValue<List<Hospital>> hospitals,
  }) = _MainState;
}

var mainProvider = StateNotifierProvider.autoDispose<MainNotifier, MainState>((ref) {
  return MainNotifier(ref);
});

class MainNotifier extends StateNotifier<MainState> {
  MainNotifier(this.ref) : super(MainState()) {
    initialProcess();
  }

  AutoDisposeStateNotifierProviderRef<MainNotifier, MainState> ref;

  void initialProcess() {
    getHospitals();
  }

  Future<void> getHospitals() async {
    try {
      final hospitals = await ref.read(hospitalGatewayProvider).getHospitals();
      state = state.copyWith(hospitals: AsyncValue.data(hospitals));
    } on Exception catch (ex) {
      state = state.copyWith(hospitals: AsyncValue.error(ex, StackTrace.current));
    }
  }
}
