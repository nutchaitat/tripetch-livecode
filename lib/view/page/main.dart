import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:tripetch_livecode/domain/entity/hospital.dart';
import 'package:tripetch_livecode/interactor/main.dart';
import 'package:tripetch_livecode/view/component/card.dart';
import 'package:tripetch_livecode/view/component/indicator.dart';

part 'main.freezed.dart';

part 'main.provider.dart';

class MainPage extends ConsumerStatefulWidget {
  const MainPage({super.key});

  static const name = 'main';

  @override
  ConsumerState<ConsumerStatefulWidget> createState() {
    return MainPageState();
  }
}

class MainPageState extends ConsumerState<MainPage> {
  @override
  Widget build(BuildContext context) {
    final state = ref.watch(mainProvider);
    return Scaffold(
        appBar: AppBar(
          title: const Text('Tripetch Live Coding Session'),
        ),
        body: SafeArea(
          child: state.hospitals.map(data: (data) {
            return SingleChildScrollView(
              child: Column(
                children: data.value
                    .asMap()
                    .entries
                    .map(
                      (entity) => FadeIn(
                        delay: Duration(milliseconds: entity.key * 100),
                        child: AppCard(title: entity.value.name),
                      ),
                    )
                    .toList(),
              ),
            );
          }, loading: (loading) {
            return const Align(child: AppIndicator());
          }, error: (error) {
            return Text(error.error.toString());
          }),
        ));
  }
}
