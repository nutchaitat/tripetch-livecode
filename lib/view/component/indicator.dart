import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tripetch_livecode/view/application.dart';

class AppIndicator extends ConsumerWidget {
  const AppIndicator({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final theme = ref.read(themeProvider);
    return CircularProgressIndicator(color: theme.colorAppBar);
  }
}
