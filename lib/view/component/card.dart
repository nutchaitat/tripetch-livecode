import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tripetch_livecode/view/application.dart';

class AppCard extends ConsumerWidget {
  const AppCard({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final theme = ref.read(themeProvider);
    return SizedBox(
      width: double.infinity,
      height: 60,
      child: Card(
        color: theme.colorCard,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Row(children: [
            Icon(Icons.local_hospital_outlined, color: theme.colorText),
            const SizedBox(width: 10),
            Flexible(
              child: FittedBox(
                child: Text(title, textAlign: TextAlign.left, style: TextStyle(color: theme.colorText, fontSize: 30)),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
