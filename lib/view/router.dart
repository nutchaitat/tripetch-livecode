import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:tripetch_livecode/view/page/main.dart';

final routerProvider = Provider<GoRouter>((ref) {
  final routes = [
    GoRoute(
      path: '/',
      name: MainPage.name,
      builder: (context, state) => const MainPage(),
    ),
  ];
  return GoRouter(
    initialLocation: '/',
    routes: routes,
  );
});
