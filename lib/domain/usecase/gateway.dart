import 'package:tripetch_livecode/domain/entity/hospital.dart';

abstract class GatewayUsecase {
  Future<List<Hospital>> getHospitals();
}
