import 'package:dio/dio.dart';
import 'package:tripetch_livecode/domain/entity/hospital.dart';
import 'package:tripetch_livecode/domain/usecase/gateway.dart';

class ApiGateway implements GatewayUsecase {
  ApiGateway._internal(this.client);

  factory ApiGateway.create() {
    final dio = Dio(BaseOptions(baseUrl: 'https://api.jsonbin.io'));
    return ApiGateway._internal(dio);
  }

  final Dio client;

  @override
  Future<List<Hospital>> getHospitals() async {
    final response = await client.get('/v3/b/639982fe01a72b59f2300654');
    if (response.data?['record']['hospitals']['hospitals'] is! List<dynamic>) {
      throw ArgumentError('format data not correct');
    }
    final results = response.data?['record']['hospitals']['hospitals'] as List<dynamic>;
    return results.map((entity) => Hospital.fromJson(entity)).toList();
  }
}
