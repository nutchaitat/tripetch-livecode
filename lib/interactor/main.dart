import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:tripetch_livecode/domain/usecase/gateway.dart';
import 'package:tripetch_livecode/interactor/gateway/api.dart';

final hospitalGatewayProvider = Provider<GatewayUsecase>((ref) {
  return ApiGateway.create();
});
